/*
  ########################################################################################################

  FASTA2AGP: creating AGP and FASTA contig sequence files from a FASTA-formatted scaffold sequence file

  Copyright (C) 2015-2021  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/
import java.io.*;
import java.util.*;
public class FASTA2AGP {
    // constants
    final static String VERSION = "2.0.210111ac";
    final static String  NOFILE = "N.o.F.i.L.e";
    // options
    static File infile;  // -i
    static File ctgFile; // -o
    static File agpFile; // -a
    static int polyNlgt; // -n
    static int mincl;    // -c
    // io
    static BufferedReader in;
    static BufferedWriter outc, outa;
    // data
    static int size;
    static ArrayList<String> fh, sq;
    // stuffs
    static int o, i, s, sN, eN, l, cptctg, part_number, base;
    static String line, hdr, polyN;
    static StringBuilder sb;
    public static void main(String[] args) throws IOException {
	// ############################
	// ### man              #######
	// ############################
	if ( args.length < 2 ) {
	    System.out.println(""); System.out.println(" FASTA2AGP v." + VERSION + "   Copyright (C) 2015-2021  Institut Pasteur");
	    System.out.println(""); System.out.println(" Usage:  FASTA2AGP  -i <scaffolds.fasta>  [-o <contigs.fasta>]  [-a <info.agp>]  [-n <length>]  [-c <length>]");
	    System.out.println(""); System.out.println(" Options:"); 
	    System.out.println("   -i <infile>   FASTA-formatted scaffold sequence file (mandatory)");
	    System.out.println("   -o <outfile>  FASTA-formatted contig sequence output file name (default: <infile>.fna)");
	    System.out.println("   -a <outfile>  AGP-formatted output file name (default: <infile>.agp)");
	    System.out.println("   -n <integer>  minimum length of scaffolding stretch of Ns (default: 10)");
	    System.out.println("   -c <integer>  minimum length of a contig sequence (default: 201)");
	    System.out.println(""); System.exit(0);
	}
	// ############################
	// ### parsing options  #######
	// ############################
	infile   = new File(NOFILE); 
	ctgFile  = new File(NOFILE);
	agpFile  = new File(NOFILE);
	polyNlgt = 10;
	mincl = 201;
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") ) { infile  = new File(args[++o]); continue; }
	    if ( args[o].equals("-o") ) { ctgFile = new File(args[++o]); continue; }
	    if ( args[o].equals("-a") ) { agpFile = new File(args[++o]); continue; }
	    if ( args[o].equals("-n") ) {
		try { polyNlgt = Integer.parseInt(args[++o]); }
		catch ( NumberFormatException e ) { System.out.println("incorrect integer value: " + args[o] + " (option -n)"); System.exit(1); }
		if ( polyNlgt < 1 ) { System.out.println("incorrect integer value: " + polyNlgt + " (option -n)"); System.exit(1); }
		continue;
	    }
	    if ( args[o].equals("-c") ) {
		try { mincl = Integer.parseInt(args[++o]); }
		catch ( NumberFormatException e ) { System.out.println("incorrect integer value: " + args[o] + " (option -c)"); System.exit(1); }
		if ( mincl < 1 ) { System.out.println("incorrect integer value: " + mincl + " (option -c)"); System.exit(1); }
		continue;
	    }
	}
	if ( ! infile.exists() ) { System.out.println(infile.toString() + " not found (options -i)"); System.exit(1); }
	if ( ctgFile.toString().equals(NOFILE) ) ctgFile = new File(infile.toString() + ".fna");	
	if ( agpFile.toString().equals(NOFILE) ) agpFile = new File(infile.toString() + ".agp");
	sb = new StringBuilder(""); s = polyNlgt; while ( --s >= 0 ) sb = sb.append('N'); polyN = sb.toString();	
	// ############################
	// ### reading infile  ########
	// ############################
	fh = new ArrayList<String>(); sq = new ArrayList<String>(); sb = new StringBuilder(""); in = new BufferedReader(new FileReader(infile));
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith(">") ) { if ( sb.length() != 0 ) { sq.add(sb.toString().toUpperCase()); sb = new StringBuilder(""); } fh.add(line); continue; }
	    sb = sb.append(line);
	}
	if ( sb.length() != 0 ) sq.add(sb.toString().toUpperCase());
	size = fh.size();
	// ############################
	// ### editing sequences  #####
	// ############################
	i = size;
	while ( --i >= 0 ) {
	    if ( (l=sq.get(i).length()) < mincl ) { fh.remove(i); sq.remove(i); continue; }
	    if ( sq.get(i).indexOf(polyN) == -1 ) continue;
	    sb = new StringBuilder(sq.get(i));
	    // ===================================================
	    //  0                  sN               eN
	    //  |                  |                |
	    //  AACTGTCACTACGAATGCTNNNNNNNNNNNNNNNNNACGTA...
	    //   => trimming seq[0, eN] if sN < mincl
	    while ( ((sN=sb.indexOf(polyN)) < mincl) && (sN != -1) ) { 
		eN = sN; while ( (++eN < l) && (sb.charAt(eN) == 'N') ) {}
		if ( (l=(sb=sb.delete(0, eN)).length()) < mincl ) break;
	    }
	    if ( l < mincl ) { fh.remove(i); sq.remove(i); continue; }
	    if ( sN == -1 ) { sq.set(i, sb.toString()); continue; }
	    // ===================================================
	    //        eN      sN       sN+polyNlgt-1        l-1
	    //        |       |        |                    |
	    //  ...AGCTNNNNNNNNNNNNNNNNNACGTACGTAACTGTGAATGCT
	    //   => trimming seq[eN+1, l] if l-sN-polyNlgt < mincl
	    while ( (l-(sN=sb.lastIndexOf(polyN))-polyNlgt < mincl) && (sN != -1) ) {
		eN = sN; while ( (--eN > 0) && (sb.charAt(eN) == 'N') ) {}
		if ( (l=(sb=sb.delete(++eN, l)).length()) < mincl ) break;
	    }
	    if ( l < mincl ) { fh.remove(i); sq.remove(i); continue; }
	    if ( sN == -1 ) { sq.set(i, sb.toString()); continue; }
	    // ==================================================================
	    //                          eN              sN
	    //                          |               |
	    //  ...AGCTNNNNNNNNNNNNNNNNNACGTACGTAACTGTGANNNNNNNNNNNNNNNNNATGCT...
	    //   => replacing characters between eN and sN-1 by 'N' if sN-eN < mincl
	    eN = sb.indexOf(polyN); while ( (++eN < l) && (sb.charAt(eN) == 'N') ) {}
	    while ( (sN=sb.indexOf(polyN, eN)) != -1 ) {
		if ( sN - eN < mincl ) { --eN; while ( ++eN < sN ) sb.setCharAt(eN, 'N'); }
		eN = sN; while ( (++eN < l) && (sb.charAt(eN) == 'N') ) {}
	    }
	    sq.set(i, sb.toString());
	}
	size = fh.size(); o = 10; while ( o < size ) o *= 10; base = ("" + (o *= 10)).length();
	// ##############################
	// ### writing outfiles  ########
	// ##############################
	outc = new BufferedWriter(new FileWriter(ctgFile)); outa = new BufferedWriter(new FileWriter(agpFile)); cptctg = 0; i = -1;
	while ( ++i < size ) {
	    line = sq.get(i).toUpperCase(); l = line.length(); s = 0; sN = line.indexOf(polyN);
	    if ( sN == -1 ) {
		hdr = "contig_" + frmt(++cptctg,base); outc.write(">" + hdr); outc.newLine(); outc.write(line); outc.newLine();
		// =======================================================================================================================================================
		//          object                           object_beg  object_end  part_number  component_type  component_id   component_beg  component_end  orientation
		//          |                                |           |           |            |               |              |              |              |
		outa.write("scaffold_" + frmt(i+1,base) + "\t1\t" +      l +      "\t1\t" +      "W\t" +          hdr +       "\t1\t" +         l +         "\t+"); outa.newLine();
		continue;
	    }
	    part_number = 0;
	    while ( sN != -1 ) {
		eN = sN; while ( (++eN < l) && (line.charAt(eN) == 'N') ) {}
		// ========================================================
		//        s                  sN               eN
		//        |                  |                |
		//  ...NNNAACTGTCACTACGAATGCTNNNNNNNNNNNNNNNNNACGTACGT
		hdr = "contig_" + frmt(++cptctg,base); outc.write(">" + hdr); outc.newLine(); outc.write(line.substring(s, sN)); outc.newLine();
		// =====================================================================================================================================================================
		//          object                               object_beg     object_end  part_number          component_type  component_id  component_beg  component_end  orientation
		//          |                                    |              |           |                    |               |             |              |              |
		outa.write("scaffold_" + frmt(i+1,base) + "\t" + (s+1) + "\t" + sN + "\t" + (++part_number) + "\tW\t" +          hdr +      "\t1\t" +         (sN-s) +    "\t+"); outa.newLine();
		// ============================================================================================================================================================
		//          object                               object_beg      object_end  part_number          component_type gap_length   gap_type  linkage   link.evidence
		//          |                                    |               |           |                    |              |            |         |         |
		outa.write("scaffold_" + frmt(i+1,base) + "\t" + (sN+1) + "\t" + eN + "\t" + (++part_number) + "\tN\t" +         (eN-sN) + "\tscaffold\tyes\t" + "paired-ends"); outa.newLine();
		s = eN; sN = line.indexOf(polyN, s);
	    }
	    // =====================================
	    //        s                  
	    //        |                  
	    //  ...NNNAACTGTCACTACGAATGCTACGTACGT
	    hdr = "contig_" + frmt(++cptctg,base); outc.write(">" + hdr); outc.newLine(); outc.write(line.substring(s)); outc.newLine();
	    // ======================================================================================================================================================================
	    //          object                               object_beg     object_end  part_number          component_type  component_id   component_beg  component_end  orientation
	    //          |                                    |              |           |                    |               |              |              |              |
	    outa.write("scaffold_" + frmt(i+1,base) + "\t" + (s+1) + "\t" + l + "\t" +  (++part_number) + "\tW\t" +          hdr +       "\t1\t" +         (l-s) +     "\t+"); outa.newLine();
	}
	outc.close(); outa.close();
    }
    final static String frmt( final int x, final int base ) { return Integer.toString((int)(Math.pow(10, base) + x)).substring(1); }
}
