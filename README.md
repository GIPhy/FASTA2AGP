# FASTA2AGP

_FASTA2AGP_ is a command line program written in [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html) to [AGP](https://www.ncbi.nlm.nih.gov/assembly/agp/AGP_Specification/) and associated contig sequence files from a FASTA-formatted scaffold sequence file.



## Compilation and execution

The source code of _FASTA2AGP_ is inside the _src_ directory and can be compiled and executed in two different ways. 

#### Building an executable jar file

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/FASTA2AGP.git
```
On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (6 or higher) installed, Java executable jar files could be created. In a command-line window, go to the _src_ directory and type:
```bash
javac FASTA2AGP.java
echo Main-Class: FASTA2AGP > MANIFEST.MF 
jar -cmvf MANIFEST.MF FASTA2AGP.jar FASTA2AGP.class 
rm MANIFEST.MF FASTA2AGP.class 
```
This will create the executable jar file `FASTA2AGP.jar` that can be run with the following command line model:
```bash
java -jar FASTA2AGP.jar <options>
```

#### Building a native code binary

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/FASTA2AGP.git
```
On computers with [GraalVM](hhttps://www.graalvm.org/downloads/) installed, native executables can be built. In a command-line window, go to the _src_ directory, and type:
```bash
javac FASTA2AGP.java
native-image FASTA2AGP FASTA2AGP
```
This will create the native executable `FASTA2AGP` that can be run with the following command line model:
```bash
./FASTA2AGP <options>
```

## Usage

Run _FASTA2AGP_ without option to read the following documentation:

```
 Usage:  FASTA2AGP  -i <scaffolds.fasta>  [-o <contigs.fasta>]  [-a <info.agp>]  [-n <length>]  [-c <length>]

 Options:
   -i <infile>   FASTA-formatted scaffold sequence file (mandatory)
   -o <outfile>  FASTA-formatted contig sequence output file name (default: <infile>.fna)
   -a <outfile>  AGP-formatted output file name (default: <infile>.agp)
   -n <integer>  minimum length of scaffolding stretch of Ns (default: 10)
   -c <integer>  minimum length of a contig sequence (default: 201)
```

## Example

The directory _example/_ contains the file _scaffolds.fasta_.
This FASTA file is made up by five small scaffold sequences, some of them containing stretches of Ns inferred from paired-end sequencing data when scaffolding assembled contigs.
An AGP file can be created, as well as the associated contig file by running the following command line:
```bash
FASTA2AGP -i scaffolds.fasta -o contigs.fasta -a contigs.agp
```
Both output files are available in the directory _example/_.
Each stretch of Ns of length at least 10 was discarded, therefore leading to different contigs (of minimum length 201 bases):

* The scaffold `seq1` yielded two contigs (and three entries in the AGP file), because of the 100-N stretch on its center.
* The scaffold `seq2` contains two stretches of at least 10 Ns, but the corresponding contigs (i.e. the two first) are of length < 201 bases, therefore leading to only one output contig (i.e. the last one).
* Same for `seq3` for which the two last contigs were discarded (< 201 bases).
* The scaffold `seq4` is made up by three contigs, but the second one was too short (< 201 bases) and replaced by Ns.
* The last scaffold `seq5` being too short, it was discarded.



